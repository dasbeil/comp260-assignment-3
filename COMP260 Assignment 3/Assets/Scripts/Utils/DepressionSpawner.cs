﻿using UnityEngine;
using System.Collections;

public class DepressionSpawner : MonoBehaviour {
    public DepressionMove depressionPrefab;
    public float xMin;
    public float yMin = 5f;
    private int sadNum = 0;
    private float timeTillNext, TimeSinceLast = 0f;
    public float minPeriod, MaxPeriod;
    public Transform target;


    // Use this for initialization
    void Start()
    {
    }
    public void createSadness()
    {
        PlayerMove p = FindObjectOfType<PlayerMove>();
        target = p.transform;

        DepressionMove SadBubble = Instantiate(depressionPrefab);

        SadBubble.transform.parent = transform;
        SadBubble.gameObject.name = "Sad Bubble" + sadNum;

        float x = target.position.x;
        float y = yMin;
        SadBubble.transform.position = new Vector3(x, y, -1);

        timeTillNext = Mathf.Lerp(minPeriod, MaxPeriod, Random.value);
        sadNum++;
        TimeSinceLast = 0f;
    }
    public void createRandSadness()
    {
        PlayerMove p = FindObjectOfType<PlayerMove>();
        target = p.transform;

        DepressionMove SadBubble = Instantiate(depressionPrefab);

        SadBubble.transform.parent = transform;
        SadBubble.gameObject.name = "Sad Bubble" + sadNum;

        float x = xMin + Random.Range(-10,10);
        float y = yMin;
        SadBubble.transform.position = new Vector3(x, y, -1);
    }
    // Update is called once per frame
    void Update () {
        TimeSinceLast += Time.deltaTime;
        if (TimeSinceLast >= timeTillNext)
        {
            createSadness();
        }
    }
    public void Reset()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            Destroy(child.gameObject);
        }
        timeTillNext = 0f;
        TimeSinceLast = 0f;
        sadNum = 0;
    }
}
