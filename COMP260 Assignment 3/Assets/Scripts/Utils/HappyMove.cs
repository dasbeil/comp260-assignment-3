﻿using UnityEngine;
using System.Collections;

public class HappyMove : MonoBehaviour {

    new AudioSource audio;
    new Rigidbody rigidbody;
    public AudioClip playerCollideClip;
    public LayerMask player;
    public LayerMask EndZone;
    private DepressionSpawner sad;
    void Start()
    {
        audio = GetComponent<AudioSource>();
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = true;
        sad = FindObjectOfType<DepressionSpawner>();
    }

    // Update is called once per frame
    void Update () {
    }
    public void Destroy()
    {
        Destroy (gameObject);
    }
    void OnCollisionEnter(Collision collision) {
        if (EndZone.Contains(collision.gameObject))
        {
            sad.createSadness();
            sad.createRandSadness();
            Destroy();
        }

        float rng = Random.Range(-4, 4);
        rigidbody.velocity = new Vector3(rng, 12, 0);

        //check what we have hit
        if (player.Contains(collision.gameObject))
        {
            //hit the paddle
            audio.PlayOneShot(playerCollideClip);

        }
    }
}
