﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItsFinalCountDown : MonoBehaviour
{
    public float timeLeft = 60.0f;
    public float loseTime = 3.0f;
    public Text text;
    public Text wintext;
    public Text highScore;

    void start()
    {
    }

    void Update()
    {
        PlayerMove player = FindObjectOfType<PlayerMove>();

        timeLeft -= Time.deltaTime;
        text.text = "Time Left: " + Mathf.Round(timeLeft);
        if (timeLeft < 0)
        {
            if (player.speed >= 40)
            {
                text.text = "YOU WIN!!!";
                wintext.text = "Bloody Hackers";
                Time.timeScale = 0.0f;
            } else if (player.speed >= 30)
            {
                text.text = "YOU WIN!";
                wintext.text = "You Did Great Mate, rated 8/8";
                Time.timeScale = 0.0f;
            } else if (player.speed >= 20)
            {
                text.text = "You Win";
                wintext.text = "Not Bad";
                Time.timeScale = 0.0f;
            } else if( player.speed >= 10)
            {
                text.text = "you win";
                wintext.text = "meh okay";
                Time.timeScale = 0.0f;
            } else if(player.speed > 0)
            {
                text.text = "you win?";
                wintext.text = "really? did you tho?";
                Time.timeScale = 0.0f;
            } else
            {
                text.text = "you lose";
                wintext.text = "too bad, so close?";
                Time.timeScale = 0.0f;
            }
         
         }
        if (player.speed == 0)
        {
            loseTime -= Time.deltaTime;
            if (loseTime <= 0)
            {
                text.text = "you lose";
                wintext.text = "too bad";
                Time.timeScale = 0.0f;
            }
        }

    }
    public void Reset()
    {
        PlayerMove player = FindObjectOfType<PlayerMove>();
        HappySpawner happySpawn = FindObjectOfType<HappySpawner>();
        DepressionSpawner sadSpawn = FindObjectOfType<DepressionSpawner>();
        player.Reset();
        highScore.text = "High Score: " + player.highScore;
        happySpawn.Reset();
        sadSpawn.Reset();
        Time.timeScale = 1.0f;
        timeLeft = 60.0f;
        loseTime = 3.0f;
        wintext.text = "";
    }
}
