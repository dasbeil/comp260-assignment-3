﻿using UnityEngine;
using System.Collections;

public class HappySpawner : MonoBehaviour {

    public HappyMove happyPrefab;
    private float xMin;
    private float yMin= 5f;
    private int happyNum = 0;
    private float timeTillNext, TimeSinceLast = 0f;
    public float minPeriod, MaxPeriod;


    // Use this for initialization
    void Start () {
        createHappy();
	}
	void createHappy()
    {
        HappyMove HappyBubble = Instantiate (happyPrefab);

        HappyBubble.transform.parent = transform;
        HappyBubble.gameObject.name = "Happy Bubble" + happyNum;

        float x = xMin + Random.Range(-10, 10);
        float y = yMin;
        HappyBubble.transform.position = new Vector3(x, y, -1);

        timeTillNext = Mathf.Lerp(minPeriod, MaxPeriod, Random.value);
        happyNum++;
        TimeSinceLast = 0f;
    }
    
    // Update is called once per frame
    void Update () {
        TimeSinceLast += Time.deltaTime;
        if (TimeSinceLast >= timeTillNext)
        {
            createHappy();
        }
    }
    public void Reset()
    {
        for(int i = 0; i<transform.childCount; i++)
        {
            Transform child = transform.GetChild(i); 
            Destroy(child.gameObject);
        }
        timeTillNext = 0f; 
        TimeSinceLast = 0f;
        happyNum = 0;
    }
}
